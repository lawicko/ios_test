//
//  nexoraUITests.swift
//  nexoraUITests
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import XCTest

class nexoraUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        app = XCUIApplication()
        app.launchEnvironment["testMode"] = "true" // can be anything really, just needs to be defined
        app.launch()

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLaunch() {
        let tableView = app.tables[AccessibilityConstants.table_view]
        XCTAssertTrue(tableView.exists, "Table view should exist")
    }
    
    func testSearch() {
        let searchPhrase = "Skywalker"
        search(query: searchPhrase)
        
        let tableView = app.tables[AccessibilityConstants.table_view]
        let numberOfCells = tableView.cells.count
        
        waitFor(object: tableView, errorMessage: "There should be only 1 cell in the table view") { (tableView) -> Bool in
            tableView.cells.count == 1
        }
        XCTAssertEqual(numberOfCells, 1, "There are only 1 record matching with \(searchPhrase) in the mock data")
    }
    
    func testPersonScene() {
        let searchPhrase = "Skywalker"
        let tableView = app.tables[AccessibilityConstants.table_view]
        search(query: searchPhrase)
        waitFor(object: tableView, errorMessage: "There should be only 1 cell in the table view") { (tableView) -> Bool in
            tableView.cells.count == 1
        }
        let cell = tableView.cells.firstMatch
        cell.tap()
        
        let nameLabel = app.staticTexts.element(matching: NSPredicate(format: "label contains 'name: Luke Skywalker'"))
        XCTAssertTrue(nameLabel.waitForExistence(timeout: 5), "The name label should exist")
    }
    
    func testError() {
        search(query: "jakub")
        let alert = app.alerts.firstMatch
        XCTAssertTrue(alert.waitForExistence(timeout: 5), "When searching for the non existing character, the error alert should be shown")
    }

//    func testLaunchPerformance() {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
//        }
//    }
}

extension nexoraUITests {
    func search(query: String) {
        let searchField = app.searchFields[AccessibilityConstants.search_field]
        XCTAssertTrue(searchField.exists, "Search bar should exist")
        
        searchField.tap()
        searchField.typeText(query)
        app.keyboards.buttons["Search"].tap()
    }
}
