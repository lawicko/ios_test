//
//  nexoraUITestsUtils.swift
//  nexoraUITests
//
//  Created by Jakub Lawicki on 28 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import XCTest

extension XCTestCase {
    func waitFor<T>(object: T, timeout: TimeInterval = 5, errorMessage: String?, file: String = #file, line: UInt = #line, expectationPredicate: @escaping (T) -> Bool) {
        let predicate = NSPredicate { obj, _ in
            expectationPredicate(obj as! T)
        }
        expectation(for: predicate, evaluatedWith: object, handler: nil)

        waitForExpectations(timeout: timeout) { error in
            if (error != nil) {
                let genericMessage = "Failed to fulfill expectation block for \(object) after \(timeout) seconds."
                let message = errorMessage != nil ? genericMessage + " \(String(describing: errorMessage))" : genericMessage
                self.recordFailure(withDescription: message, inFile: file, atLine: Int(line), expected: true)
            }
        }
    }
}
