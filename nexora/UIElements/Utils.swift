//
//  Utils.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import Foundation

let isTest = ProcessInfo.processInfo.environment["testMode"] != nil

struct AccessibilityConstants {
    static let table_view = "table_view"
    static let navigation_bar = "navigation_bar"
    static let search_field = "search_field"
}
