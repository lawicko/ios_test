//
//  SubtitleTableViewCell.swift
//  nexora
//
//  Created by Jakub Lawicki on 28 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import UIKit

class SubtitleTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
