//
//  Moya+Extensions.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import Foundation
import Moya

extension TargetType {
    /// This just ensures that we always validate on the success codes
    var validationType: ValidationType {
        return .successCodes
    }
    
    /// This is just a shortcut to easily get the stubbed data
    /// - Parameter filename: a file with the stubbed json response
    func stubbedResponse(_ filename: String) -> Data! {
        guard let path = Bundle.main.path(forResource: filename, ofType: "json") else { fatalError("path could not be found: \(filename)") }
        return (try? Data(contentsOf: URL(fileURLWithPath: path)))
    }
}
