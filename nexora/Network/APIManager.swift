//
//  APIManager.swift
//  nexora
//
//  Created by Jakub Lawicki on 28 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import Foundation
import Moya
import PromiseKit

protocol GeneralAPI {
    func callApi<T: TargetType, U: Decodable>(_ target: T, dataReturnType: U.Type, test: Bool) -> Promise<U>
}

struct APIManager: GeneralAPI {
    
    /// Generic function to call API endpoints using moya and decodable protocol
    ///
    /// - Parameters:
    ///   - target: The network moya target endpoint to call
    ///   - dataReturnType: The typpe of data that is expected to parse from endpoint response
    ///   - test: Boolean that help toggle real network call or simple mock data to be returned by moya
    ///   - debugMode: Toggle the verbose mode of moya
    /// - Returns: A promise containing the dataReturnType set in function params
    func callApi<Target: TargetType, ReturnedObject: Decodable>(_ target: Target, dataReturnType: ReturnedObject.Type, test: Bool = isTest) -> Promise<ReturnedObject> {
        
        let provider = test ? (MoyaProvider<Target>(stubClosure: MoyaProvider.immediatelyStub)) : MoyaProvider<Target>()
        
        return Promise { seal in
            provider.request(target) { result in
                switch result {
                case let .success(response):
                    let decoder = JSONDecoder()
                    // Would use the ISO8601DateFormatter with the .withFractionalSeconds but it inherits from Formatter, not DateFormatter, nice job Apple
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                    
                    decoder.dateDecodingStrategy = .formatted(dateFormatter)
                    do {
                        let results = try decoder.decode(ReturnedObject.self, from: response.data)
                        seal.fulfill(results)
                    } catch {
                        seal.reject(error)
                    }
                case let .failure(error):
                    seal.reject(error)
                }
            }
        }
    }
}
