//
//  Network+Injection.swift
//  nexora
//
//  Created by Jakub Lawicki on 28 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import Foundation
import Resolver

extension Resolver {
    public static func registerNetworkServices() {
        register { APIManager() }
    }
}
