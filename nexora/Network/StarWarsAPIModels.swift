//
//  StarWarsAPIModels.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import Foundation

enum APIPersonGender: String, Codable {
    case male
    case female
    case hermaphrodite
    case unknown = "n/a"
}

struct APIPerson: Codable {
    let birth_year: String
    let created: Date
    let edited: Date
    let eye_color: String
    let films: Array<URL>
    let gender: APIPersonGender
    let hair_color: String
    let height: String
    let homeworld: URL
    let mass: String
    let name: String
    let skin_color: String
    let species: Array<URL>
    let starships: Array<URL>?
    let url: URL
    let vehicles: Array<URL>
    
    static let Luke = APIPerson(birth_year: "",
                                created: Date(),
                                edited: Date(),
                                eye_color: "",
                                films: Array<URL>(),
                                gender: .male,
                                hair_color: "",
                                height: "",
                                homeworld: URL(string: "https://swapi.co/api/planets/1/")! ,
                                mass: "",
                                name: "Luke Skywalker",
                                skin_color: "",
                                species: Array<URL>(),
                                starships: nil,
                                url: URL(string: "https://swapi.co/api/people/1/")!,
                                vehicles: Array<URL>())
}

struct APIPeopleResponse: Codable {
    let next: URL?
    let results: Array<APIPerson>
    let previous: URL?
    let count: Int
}

struct APIPlanet: Codable {
    let climate: String
    let created: Date
    let diameter: String
    let edited: Date
    let films: Array<URL>
    let gravity: String
    let name: String
    let orbital_period: String
    let population: String
    let residents: Array<URL>
    let rotation_period: String
    let surface_water: String
    let terrain: String
    let url: URL
}

struct APIPlanetsResponse: Codable {
    let next: URL?
    let results: Array<APIPlanet>
    let previous: URL?
    let count: Int
}
