//
//  StarWarsAPI.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import Foundation
import Moya

enum StarWarsAPI {
    case people(String)
    case planets(String)
}

extension StarWarsAPI: TargetType {
    var baseURL: URL {
        return URL(string: StarWarsAPIConfig.baseUrl)!
    }
    
    var path: String {
        switch self {
        case .people:
            return "people"
        case .planets:
            return "planets"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .people, .planets:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .people(let query):
            switch query {
            case "skywalker", "Skywalker":
                return stubbedResponse("people_mock_skywalker")
            case "Jakub", "jakub":
                return Data()
            default:
                return stubbedResponse("people_mock")
            }
        case .planets(let query):
            switch query {
            case "skywalker", "Skywalker":
                return Data()
            default:
                return stubbedResponse("planets_mock")
            }
        }
    }
    
    var task: Task {
        switch self {
        case .people(let query):
            return .requestParameters(parameters: ["search": query], encoding: URLEncoding.queryString)
        case .planets(let query):
            return .requestParameters(parameters: ["search": query], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    
}
