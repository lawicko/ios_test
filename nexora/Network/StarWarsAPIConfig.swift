//
//  StarWarsAPIConfig.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import Foundation

struct StarWarsAPIConfig {
    static let baseUrl = "https://swapi.co/api/"
}
