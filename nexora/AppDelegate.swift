//
//  AppDelegate.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright © 2020 Secret Inc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let navigationController = UINavigationController(rootViewController: MainViewController(nibName: "MainViewController", bundle: nil))
        navigationController.navigationBar.accessibilityIdentifier = AccessibilityConstants.navigation_bar
        window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        return true
    }
}

