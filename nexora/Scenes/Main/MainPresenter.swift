//
//  MainPresenter.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright (c) 2020 Secret Inc.. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MainPresentationLogic {
    func presentData(response: Main.RequestData.Response)
}

class MainPresenter: MainPresentationLogic {
    weak var viewController: MainDisplayLogic?
    
    // MARK: presentData
    
    func presentData(response: Main.RequestData.Response) {
        var displayableObjects: Array<Main.Displayable>?
        if let people = response.people, let planets = response.planets {
            let displayedPeople: [Main.Displayable] = people.compactMap { Main.Displayable.person(Main.DisplayedPerson(from: $0)) }
            let displayedPlanets: [Main.Displayable] = planets.compactMap { Main.Displayable.planet(Main.DisplayedPlanet(from: $0)) }
            let displayables = displayedPeople + displayedPlanets
            let sorted = displayables.sorted { (displayable1, displayable2) -> Bool in
                var name1: String, name2: String
                switch displayable1 {
                case .person(let person):
                    name1 = person.name
                case .planet(let planet):
                    name1 = planet.name
                }
                switch displayable2 {
                case .person(let person):
                    name2 = person.name
                case .planet(let planet):
                    name2 = planet.name
                }
                return name1<name2
            }
            displayableObjects = sorted
        }
        
        let viewModel = Main.RequestData.ViewModel(displayables: displayableObjects, error: response.error)
        viewController?.displayData(viewModel: viewModel)
    }
}
