//
//  MainRouter.swift
//  nexora
//
//  Created by Jakub Lawicki on 27 Feb 20.
//  Copyright (c) 2020 Secret Inc.. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MainRoutingLogic {
    func routeToPerson(_ personURL: URL)
}

protocol MainDataPassing {
    var dataStore: MainDataStore? { get }
}

class MainRouter: NSObject, MainRoutingLogic, MainDataPassing {
    weak var viewController: MainViewController?
    var dataStore: MainDataStore?
    
    // MARK: Routing
    
    func routeToPerson(_ personURL: URL) {
        guard let person = dataStore?.people.first(where: { (person) -> Bool in
            return person.url == personURL
        }) else {
            return
        }
        
        guard let vc = viewController else {
            return
        }
        
        let personViewController = PersonViewController(nibName: "PersonViewController", bundle: nil)
        var destinationDS = personViewController.router!.dataStore!
        passDataToPerson(data: person, destination: &destinationDS)
        navigateToPerson(source: vc, destination: personViewController)
    }
    
    // MARK: Navigation
    
    func navigateToPerson(source: MainViewController, destination: PersonViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }
    
    // MARK: Passing data
    
    func passDataToPerson(data: APIPerson, destination: inout PersonDataStore) {
        destination.person = data
    }
}
