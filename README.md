# README #

Here are the constraints that were given:

Create an app that displays two screens:
- main screen with search for People and Planets from Star Wars, displaying data from https://swapi.co/documentation, sorted by name, also displaying diameter for planets and height for people
- people details screen displaying more data about people, e.g. hair, eyes; opened after clicking person item on main screen

### What tools did I use? ###

* https://github.com/Moya/Moya and https://github.com/mxcl/PromiseKit for modelling the network layer
* https://github.com/hmlongco/Resolver for dependency injection
* templates from http://clean-swift.com for the clean swift architecture implementation

### Remarks ###

If you look at the network layer, it consists of the `StarWarsAPI` and the `APIManager`. I remeber how we talked during the interview about how the api could change and how would I approach it. I wanted to show it here, I think with this implementation it would be pretty easy to swap the `StarWarsAPI` with any other api and basically keep the rest of the applicaiton intact. Because of this design the dependency injection may seem a bit redundant but I decided to do it anyway to show what I think is a good practice and a robust way of doing it in Swift.